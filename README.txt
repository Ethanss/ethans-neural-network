﻿NeuralNetwork.cs - project by Ethan Alexander Shulman http://xaloez.com/

Vanilla neural network library supporting recurrent layers and training using AdaGrad, evolution or QLearning.
 Released under MIT license.

Docs: http://xaloez.com/projects/projects/other%20projects/ethansneuralnetwork/docs/index.html
Examples: https://bitbucket.org/Ethanss/ethans-neural-network/src//examples/?at=master