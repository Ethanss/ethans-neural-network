﻿//Ethan Alexander Shulman 2017

using EthansNeuralNetwork;
using System.Drawing;


namespace MessingAround
{
    class SimpleFunction
    {

        public static void Main(string[] args)
        {

            //create neural network
            NeuralNetwork neuralNetwork = new NeuralNetwork(new NeuralNetworkLayer(1, false, null),//input layer
                                                            new NeuralNetworkLayer[] {//hidden layers
                                                                new NeuralNetworkLayer(4, false, Utils.Tanh_ActivationFunction)
                                                            },
                                                            new NeuralNetworkLayer(1, false, Utils.Rectifier_ActivationFunction)//output layer
                                                            );
            neuralNetwork.RandomizeWeightsAndBiases(0.0f, 0.0f, 0.0f, 0.5f);
            UtilsDrawing.DrawNeuralNetwork(neuralNetwork, 0.5f).Save("neuralnet.png");

            //sample simple function
            float[][] simpleFunctionValues = Utils.SampleFunction(10, simpleFunction, 0.0f, 1.0f);

            //convert into training data for neural network
            float[][] inputDat = new float[simpleFunctionValues.Length][],
                     targetDat = new float[simpleFunctionValues.Length][];
            for (int i = 0; i < inputDat.Length; i++)
            {
                inputDat[i] = new float[] {simpleFunctionValues[i][0]};
                targetDat[i] = new float[] { simpleFunctionValues[i][1] };
            }

            //train neural network
            NeuralNetworkTrainer trainer = new NeuralNetworkTrainer(neuralNetwork, inputDat, targetDat, 1, NeuralNetworkTrainer.LOSS_TYPE_AVERAGE);
            trainer.learningRate = 0.04f;
            trainer.desiredLoss = 0.01f;
            trainer.lossSmoothing = 0.0f;

            trainer.Start();

            while (trainer.Running() && System.Console.ReadLine() != "stop")
            {
                System.Console.WriteLine("Iterations: " + trainer.GetIterations() + ", " + trainer.GetLoss());
            }
            trainer.Stop();
            trainer.Join(-1);

            //sample neural network
            float[][] neuralNet = Utils.SampleNeuralNetwork(10, neuralNetwork, 0.0f, 1.0f);

            //visualize simpleFunction vs neural network
            UtilsDrawing.DrawLineGraph(new float[][][] { simpleFunctionValues, neuralNet }, new Color[] { Color.Red, Color.Blue }, new string[] { "simple function", "neural net" }, 0.0f, 1.0f, 0.0f, 1.0f).Save("graph.png");
        }


        public static float simpleFunction(float x)
        {
            float v = x*x*2.0f-x*0.5f;
            if (v < 0.0f) return 0.0f;
            if (v > 1.0f) return 1.0f;
            return v;
        }
    }
}
