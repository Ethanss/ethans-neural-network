//Ethan Alexander Shulman 2017

using System;
using System.Drawing;
using System.Drawing.Imaging;
using EthansNeuralNetwork;


namespace MessingAround
{
    class ImageToShaderFunction
    {


        public /*static*/ void Main(string[] args)
        {
            //load image
            Bitmap img = new Bitmap(args[0]);

            //convert to training data(input = uv, output = RGB color)
            float[][] inputData = new float[img.Width * img.Height][],
                      targetData = new float[inputData.Length][];
            BitmapData bdat = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                byte* px = (byte*)bdat.Scan0.ToPointer();
                int stride = bdat.Stride;

                for (int x = 0; x < img.Width; x++)
                {
                    for (int y = 0; y < img.Height; y++)
                    {
                        int dataIndex = x + y * img.Width;
                        
                        //image uv
                        inputData[dataIndex] = new float[] {x/(float)(img.Width-1),y/(float)(img.Height-1)};

                        //image pixel color
                        int pixelIndex = x*3+y*stride;
                        targetData[dataIndex] = new float[] {px[pixelIndex]/255.0f,
                                                             px[pixelIndex+1]/255.0f,
                                                             px[pixelIndex+2]/255.0f};
                    }
                }
            }


            //build neural net
            NeuralNetwork neuralNetwork = new NeuralNetwork(new NeuralNetworkLayer(2, false, null),//2D coordinate input
                                                            new NeuralNetworkLayer[] {
                                                                new NeuralNetworkLayer(5, false, Utils.Rectifier_ActivationFunction)//one small hidden layer
                                                            },
                                                            new NeuralNetworkLayer(3, false, Utils.Rectifier_ActivationFunction));//RGB color output
            neuralNetwork.RandomizeWeightsAndBiases(0.0f, 0.0f, 0.0f, 1.0f / 5.0f);


            //train neural network
            NeuralNetworkTrainer trainer = new NeuralNetworkTrainer(neuralNetwork, inputData, targetData, 1, NeuralNetworkTrainer.LOSS_TYPE_AVERAGE);
            trainer.lossSmoothing = 0.0f;
            trainer.desiredLoss = 0.0f;
            trainer.learningRate = 0.04f;
            trainer.shuffleChance = 1.0f;
            //shuffling the training data helps
            Utils.Shuffle(inputData, targetData);


            trainer.Start();

            while (trainer.Running())
            {
                trainer.desiredLoss = trainer.GetIterations()/1000.0f;
                System.Threading.Thread.Sleep(10);
            }
            trainer.Stop();
            trainer.Join(-1);

            //save trained neural network as GLSL function
            System.IO.File.WriteAllText("image_as_glsl.txt", Utils.AsShader(neuralNetwork, "imageNeuralNet", true));
        }


    }
}
